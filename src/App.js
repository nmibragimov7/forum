import Main from "./containers/Main";
import { Container } from "react-bootstrap";
import { Switch, Route, Redirect } from "react-router-dom";
import Toolbar from "./components/Toolbar";
import Posts from "./containers/Posts";
import Profile from "./containers/Profile";
import Login from "./containers/Login";
import { useSelector } from "react-redux";

function App() {
  const user = useSelector(store => store.users.user);

  return (
    <>
      <header>
        <Toolbar user={user}/>
      </header>
      <main>
        <Container>
          <Switch>
            <Route exact path="/" component={Main}/>
            <Route exact path="/posts" component={Posts}/>
            <Route exact path="/login" component={Login}/>
            <ProtectRoute path="/profile" isAllowed={user} redirectTo={'/login'} exact component={Profile}/>
            <Route path='/' render={()=>(<div><h1>404 Not found</h1></div>)}/>
          </Switch>
        </Container>
      </main>
    </>
  );
};

const ProtectRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ? <Route {...props}/> : <Redirect to={redirectTo}/>
};

export default App;
