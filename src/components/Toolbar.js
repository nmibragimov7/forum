import { Navbar, Nav, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { logoutUser } from "../store/actions/usersAction";

const Toolbar = ({user}) => {
    const dispatch = useDispatch();
    const logout = () => {
        dispatch(logoutUser())
    };

    return (
        <Navbar bg="light" variant="light">
        <Navbar.Brand href="/">Forum</Navbar.Brand>
        <Nav>
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/posts">Posts</Nav.Link>
            <Nav.Link href="/profile">Profile</Nav.Link>
        </Nav>
        {user && (
            <>
                <span className="ml-auto">{user.name}</span>
                <Button className="ml-2" onClick={logout}>Log out</Button>
            </>
        )}
    </Navbar>
    )
};

export default Toolbar;