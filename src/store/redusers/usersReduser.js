import {
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER,
    RESET_ERROR
} from "../actions/usersAction";

const initialState = {
    errors: null,
    user: null,
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return {...state, errors: null, user: action.user};
        case LOGIN_USER_FAILURE:
            return {...state, errors: action.errors};
        case RESET_ERROR:
            return {...state, registerError: null, errors: null};
        case LOGOUT_USER:
            return {...state, user: null};
        default:
            return state;
    }
};

export default usersReducer;