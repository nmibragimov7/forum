import axios from "axios";
import { push } from "connected-react-router";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_FAILURE = "LOGIN_USER_FAILURE";
export const RESET_ERROR = "RESET_ERROR";
export const LOGOUT_USER = 'LOGOUT_USER';

export const resetError = () => {
    return {type: RESET_ERROR}
};

export const loginUserSuccess = user => {
    return {type: LOGIN_USER_SUCCESS, user}
};

export const loginUserFailure = errors => {
    return {type: LOGIN_USER_FAILURE, errors}
};

export const loginUser = (userData) => {
    return async (dispatch, getState) => {
        try {
            if(userData.username === "Admin" && userData.password === "12345") {
                const response = await axios.get('https://jsonplaceholder.typicode.com/users/1');
                dispatch(loginUserSuccess(response.data));
                dispatch(push('/'));
            } 
            else {
                dispatch(loginUserFailure({
                    errors: "Имя пользователя или пароль введены не верно"
                }));
            }
        } catch (e) {
            console.error(e);
        }
    }
};

export const logoutUser = () => {
    return async dispatch => {
        try {
            dispatch({type: LOGOUT_USER});
            dispatch(push('/'));
        } catch (e) {
            console.error(e);
        }
    }
};