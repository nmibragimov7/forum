import axios from 'axios';
import { loadingHandler, loadingOffHandler } from "./loadingActions";

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';

export const fetchPostsSuccess = (posts) => {
    return {type: FETCH_POSTS_SUCCESS, posts}
};

export const fetchPosts = () => {
    return async dispatch => {
        dispatch(loadingHandler());
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/posts');
            dispatch(fetchPostsSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};