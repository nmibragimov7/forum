import axios from 'axios';
import { loadingHandler, loadingOffHandler } from "./loadingActions";

export const FETCH_PHOTOS_SUCCESS = 'FETCH_PHOTOS_SUCCESS';

export const fetchPhotosSuccess = (photos) => {
    return {type: FETCH_PHOTOS_SUCCESS, photos}
};

export const fetchPhotos = () => {
    return async dispatch => {
        dispatch(loadingHandler());
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/photos');
            dispatch(fetchPhotosSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};