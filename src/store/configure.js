import { createBrowserHistory } from "history";
import { combineReducers, applyMiddleware, compose, createStore } from "redux";
import { connectRouter, routerMiddleware } from 'connected-react-router';
import thunkMiddleware from 'redux-thunk';
import { loadFromLocalStorage, saveToLocalStorage } from "./localStorage";
import photosReducer from "./redusers/photosReducer";
import postsReducer from "./redusers/postsReduser";
import loadingReduser from "./redusers/loadingReducer";
import usersReducer from "./redusers/usersReduser";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const history = createBrowserHistory();

const rootReducer = combineReducers({
    loading: loadingReduser,
    photos: photosReducer,
    posts: postsReducer,
    users: usersReducer,
    router: connectRouter(history)
});

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadFromLocalStorage();

export const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(()=> {
    saveToLocalStorage({
        users:{
            user: store.getState().users.user
        }
    });
});