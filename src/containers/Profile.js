import { useSelector } from "react-redux";

const Profile = () => {
    const user = useSelector(store => store.users.user);
    return (
        <>
            <h1>{user && user.name}</h1>
            <p>email: {user && user.email}</p>
            <p>address: {user && user.address.city}</p>
            <p>phone: {user && user.phone}</p>
        </>
    )
};

export default Profile;