import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchPosts } from "../store/actions/postsActions";
import { Card, Row, Col, Spinner } from "react-bootstrap";

const Posts = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts.posts);
    const loading = useSelector(state => state.loading.loading);
    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    return (
        <>
            {loading ? (
                <Row>
                    <Col xs={1}  className="ml-auto mr-auto mt-5">
                        <Spinner animation="border" variant="primary"/>
                    </Col>
                </Row>
            ) : (
                <Row noGutters={true}>
                    {posts.map((post, i) => {
                        if(i < 20) {
                            return (
                                <Col key={i} xs={12} md={6}>
                                    <Card border="light" style={{height: "100%"}}>
                                        <Card.Body>
                                            <Card.Title>{post.title}</Card.Title>
                                            <Card.Text>{post.body}</Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        }
                        return null;
                    })}
                </Row>
            )}
        </>
    );
}

export default Posts;