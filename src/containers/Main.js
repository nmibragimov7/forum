import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchPhotos } from "../store/actions/photosActions";
import { Card, Row, Col, Spinner } from "react-bootstrap";

const Main = () => {
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);
    const loading = useSelector(state => state.loading.loading);
    useEffect(() => {
        dispatch(fetchPhotos());
    }, [dispatch]);

    return (
        <>
            {loading ? (
                <Row>
                    <Col xs={1}  className="ml-auto mr-auto mt-5">
                        <Spinner animation="border" variant="primary"/>
                    </Col>
                </Row>
            ) : (
                <Row noGutters={true}>
                    {photos.map((photo, i) => {
                        if(i < 20) {
                            return (
                                <Col key={i} xs={12} md={6} lg={4}>
                                    <Card border="light">
                                        <Card.Img variant="top" src={photo.url} />
                                    </Card>
                                </Col>
                            )
                        }
                        return null;
                    })}
                </Row>
            )}
        </>
    );
}

export default Main;