import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Button, Row, Col, Alert } from "react-bootstrap";
import { loginUser, resetError } from "../store/actions/usersAction";

const Login = () => {
    const [state, setState] = useState({
        username: "",
        password: ""
    });
    const errors = useSelector(store => store.users.errors);
    const dispatch = useDispatch();
    const inputHandler = (e) => {
        const {name, value} = e.target;
        setState(prevState => {
            return {...prevState, [name]: value}
        });
    };
    const submit = (e) => {
        e.preventDefault();
        dispatch(loginUser(state));
    };
    const closeAlert = () => {
        dispatch(resetError());
    };
    useEffect(() => {
        dispatch(resetError());
    }, [dispatch])

    return (
        <>
            {errors && (
                <Alert key="errors" variant="warning">
                    <div className="d-flex justify-content-between align-items-center">
                        <span>{errors.errors}</span>
                        <Button onClick={closeAlert} variant="link">Close</Button>
                    </div>
                </Alert>
            )}
            <Row>
                <Col xs={12} md={6} className="ml-auto mr-auto mt-5 bg-light p-5">
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" name="username" placeholder="Enter username" onChange={inputHandler}/>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password" placeholder="Password" onChange={inputHandler}/>
                        </Form.Group>
                        <Button variant="primary" type="submit" onClick={submit}>
                            Login
                        </Button>
                    </Form>
                </Col>
            </Row>
        </>
    );
};

export default Login;